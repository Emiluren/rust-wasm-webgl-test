#[macro_use]
extern crate stdweb;

use stdweb::web::{document, html_element, IParentNode};
use stdweb::unstable::TryInto;
use webgl_stdweb::WebGL2RenderingContext;

fn main() {
    let canvas: html_element::CanvasElement =
        document().query_selector("#glCanvas").unwrap().unwrap().try_into().unwrap();
    let gl: WebGL2RenderingContext = canvas.get_context().unwrap();

    gl.clear_color(0.0, 0.0, 0.0, 1.0);
    gl.clear(WebGL2RenderingContext::COLOR_BUFFER_BIT);

    js! {
        console.log("Context created");
    }
}
